/* SPDX-License-Identifier: GPL-2.0-only */

#ifndef _BINDINGS_GOOGLE_HSUART_H
#define _BINDINGS_GOOGLE_HSUART_H

#define HSUART1 qupv3_se7_2uart_a

#endif /* _BINDINGS_GOOGLE_HSUART_H */
