#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Generate GKI kernel and P11-specific kernel binaries

DEVICE_KERNEL_BUILD_CONFIG="private/devices/google/p11/build.config.p11" \
  BUILD_KERNEL=${BUILD_KERNEL:-0} \
  private/google-modules/soc/msm/scripts/build_mixed.sh "$@"
